from tweepy import OAuthHandler, Stream
from tweepy.streaming import StreamListener

# Set up Tweepy StreamListener
# This is the version *without* unnecessary newlines
class TweetListener(StreamListener):
    def on_data(self, raw_data):
        try:
            with open('trump-data2.json', 'a') as f:
                f.write(raw_data.rstrip() + '\n')
                return True
        except BaseException as e:
            print("Error on_data: %s" % str(e))

    def on_error(self, status_code):
        print(status_code)
        return True

# Xes as not to expose actual secrets
consumer_key = 'xxxxxxxxxxxxxxxxxxxxxxxx' 
consumer_secret = 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'
access_token = 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'
access_secret = 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'

# Set up Tweepy OAuth authentication
auth = OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_secret)

# Start streaming tweets
# In order to regex the results, just add the regexes in the
# on_data function in TweetListener
twitter_stream = Stream(auth, TweetListener())
twitter_stream.filter(track=['trump'])