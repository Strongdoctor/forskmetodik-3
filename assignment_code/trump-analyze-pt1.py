import json
import re
import os
import pandas as pd
import matplotlib.pyplot as plt

class Assignment2:

    # The raw tweets json data
    tweets = None

    # The tweets json data, converted to a Pandas dataframe
    tweets_df = None

    # Key: search-phrase shortened
    # Value: ´occurrences of certain words
    word_counts = dict()

    def __init__(self, input_file):
        self.tweets = self.load_tweets(input_file)
        self.tweets_df = self.build_dataframe(self.tweets)
        self.word_counts = self.update_word_count(self.tweets_df)
        self.create_plots(self.word_counts)

    # Load the json file
    def load_tweets(self, file):
        print("Loading tweets [1/4]")
        with open(file, 'r') as f:
            tweets = (json.loads(line) for line in f.readlines())
        return tweets

    # Updates data within the function, returns tweet amount
    def build_dataframe(self, tweets) -> pd.DataFrame:
        print("Building Pandas dataframe [2/4]")

        # Keep track of how many tweets we've gone through
        listOfTweets = list()

        # Add all tweets that actually have a text property,
        # then convert the tweets to a pandas dataframe
        for tweet in tweets:
            if "text" in tweet:
                listOfTweets.append(tweet["text"].lower())

        return pd.DataFrame({"tweet": listOfTweets})

    # Actually count the word occurrences
    def update_word_count(self, df):
        print("Count word occurrences [3/4]")

        wc = dict()
        wc["good"] = df[df["tweet"].str.contains("good")].size
        wc["bad"] = df[df["tweet"].str.contains("bad")].size
        wc["maga"] = df[df["tweet"].str.contains("make america great again")].size
        wc["nk"] = df[df["tweet"].str.contains("north korea")].size
        wc["mex"] = df[df["tweet"].str.contains("mexico")].size
        wc["fakenews"] = df[df["tweet"].str.contains("fake news")].size
        wc["wall"] = df[df["tweet"].str.contains("wall")].size
        wc["hillary"] = df[df["tweet"].str.contains("hillary")].size
        return wc

    # Output matplotlib plots
    def create_plots(self, wc):
        print("Rendering plot [4/4]")

        # Just to set figure size to not be too cramped
        fig, ax = plt.subplots(figsize=(10, 5))

        # Set plot title
        plt.title("Occurence of words in " + str(self.tweets_df.size) + " tweets (2018-05-02)")

        # Create a bar plot with len(wc) many bars to represent wc.values()
        plt.bar(range(len(wc)), wc.values())

        # Set the horizontal tickers
        plt.xticks(range(len(wc)), ('Good', 'Bad', 'MAGA', 'North Korea', 'Mexico', 'Fake News', 'Wall', 'Hillary'))

        # Save the figure
        plt.savefig('word_occurence.png', format='png')

        # Show the figure in a window
        plt.show()


assignment2 = Assignment2('trump-data-converted.json')

