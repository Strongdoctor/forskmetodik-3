# Forskningsmetodik Uppgift 3

Vi har gjort steg 1/5 och 3/5, dock är 5/5 trivial att implementera.

## 1/5
test.pdf som outputtas av koden finns i roten av detta git-repo, bilderna som outputtas finns i "given_code" mappen som bevias att det har körts.

## 3/5
Vi har denna kod spriden genom .py filer i "assignment_code" mappen. Själva datamining-koden finns i "assignment-code/datafarming.py".

Koden som bearbetar datan och outputtar en plot finns i "assignment-code/trump-analyze-pt1.py".

Kod som tar bort extra newlines (Ett problem vi hade först) finns i "assignment-code/trump-convert.py".

Frågorna vi skulle svara på finns i "assignment-code/fragor_1.txt".

## 5/5
Modifiera TweetListener.on_data() metoden i "assignment-code/datafarming.py" så att man kollar "raw_data" mot regexes.